from app import app
from flask import request
from transformers import AutoTokenizer

@app.route("/", methods=["POST"])
@app.route("/index", methods=["POST"])
def index():
    tokenizer = AutoTokenizer.from_pretrained("facebook/blenderbot_small-90M")
    text = request.form.get("input")