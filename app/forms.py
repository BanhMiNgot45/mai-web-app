from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class UserInput(FlaskForm):
    input = StringField("Type your response here: ", validators=[DataRequired()])
    submit = SubmitField("Submit")